;;;; url-fetch.asd

(asdf:defsystem #:url-fetch
  :serial t
  :description "Illustrative url-fetching program for a blog post."
  :author "Kyle Isom <coder@kyleisom.net>"
  :license "ISC license"
  :depends-on (#:drakma
               #:getopt)
  :components ((:file "package")
               (:file "url-fetch")))
