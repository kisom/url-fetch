;;;; url-fetch.lisp

(in-package #:url-fetch)

;;; "url-fetch" goes here. Hacks and glory await!

(defparameter *opts* '(("o" :required)
                       ("h" :none)))

(defmacro empty-p (seq)
  `(= (length ,seq) 0))

(defun request-url (url)
  (format t "[+] fetching ~A~%" url)
  (multiple-value-list
   (drakma:http-request  url
                        :want-stream nil)))

(defun fetch-url (url &optional (write-to-file nil))
  (unless (empty-p url)
          (let ((res (request-url url)))
            (cond ((= (cadr res) 404) (not-found url))
                  ((= (cadr res) 200) (dump-url (car res) write-to-file))
                  (:else (dump-url res))))))

(defun not-found (url)
  (format t "[!] got a 404 fetching ~A~%" url)
  (sb-ext:quit :unix-status 1))

(defun dump-url (page &optional (write-to-file nil))
  (if write-to-file
      (dump-url-to-file page write-to-file)
      (dump-url-to-stdout page)))

(defun dump-url-to-stdout (page)
  (format t "~A~%" page))

(defun dump-url-to-file (page file-name)
  (with-open-file (out (pathname file-name)
                       :direction :output
                       :if-exists :supersede)
    (with-standard-io-syntax
      (princ page out))))

(defun help-message ()
  (format t "usage: ~A [-h] [-o filename] url~%" (car sb-ext:*posix-argv*))
  (format t "options:~%")
  (format t "    -h            print this help message~%")
  (format t "    -o filename   write url to filename~%~%")
  (sb-ext:quit :unix-status 0))

(defun main ()
  (let ((argv (subseq sb-ext:*posix-argv* 1))
        (write-to-file nil))
    (multiple-value-bind (args opts)
        (getopt:getopt argv *opts*)
      (when (empty-p (car argv))
        (help-message))
      (dolist (opt opts)
        (case (car opt)
          ("h" (help-message))
          ("o" (setf write-to-file (cdr opt)))))
      (fetch-url (car args) write-to-file))))
